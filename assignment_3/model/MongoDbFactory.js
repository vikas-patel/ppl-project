/// all rightes reserved 
/// author: vikas patel
/// email:vikas.k@daffodilsw.com
/// company: daffodil
var mongoose = require('mongoose');

var state = {
  db: null,
}

exports.connect = function(url, done) {
  if (state.db) return done();

    state.db = mongoose.connection;
    mongoose.connect(url);
        
    state.db.on('error', done);
    state.db.on('open', done);
}

exports.get = function() {
  return state.db;
}

exports.close = function(done) {
  if (state.db) {
      state.db.close(function(err, result) {
      state.db = null;
      state.mode = null;
      done(err);
    })
  }
}


//functions for CRUD operations

// Get the documents collection
// this function take three arguments
// 1: query : condition
// 2: model : collection(table)
// 3: and a callback
exports.select = function(query,model,callback) {
             model.find(query,function(err,result) {
             callback(err,result);               
            });
   }
 
// insert row
// model :table name in which you want to save
exports.insert = function (model,done) {
       model.save(function (err) {
          if(err)
             done(err);
          else{
               done(null);
            }  
       });     
   }
   
exports.update = function(query,model,data,callback) {
   model.findOneAndUpdate(query, {$set:data},{ 'new': true }, function(err, doc){
       console.log("update query",query,data)
    if (err) return callback(err,doc);
    return callback(null,doc);
  });
}

exports.delete = function (query,model,callback) {
   model.findOneAndRemove(query, function(err, doc){
    if (err) return callback(err,doc);
    return callback(null,doc);
  });
}

exports.deepSave = function (conditions,model,update,callback) {
    model.findOneAndUpdate(conditions, update,{ 'new': true }, function(err, doc) {
    if (err) return callback(err,doc);
    return callback(null,doc);
    });
}

exports.deepDelete = function (postId,model,update,callback) {
    model.findByIdAndUpdate(postId, update,{ 'new': true }, function(err, doc) {
    if (err) return callback(err,doc);
    return callback(null,doc);
    });
}



//Sorting
exports.sort = function (model, findCondition, sortCondition,callback) {
    model.find(findCondition).sort(sortCondition).exec(function (err, cursor) {
        if (err) return callback(err, cursor);
        return callback(null, cursor);
    });
}   