var moment = require('moment');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var blogSchema = new Schema({
    postedBy: { type: mongoose.Schema.Types.ObjectId },
    title: String,
    author: String,
    body: String,
    imageUrl:String,
    creatorImage: String,
    creatorName: String,
    type: String,
    likeby: [{ user: String, date: { type: Date, default: Date.now } }],
    likecount: Number,
    flagby: [{ user: String, date: { type: Date, default: Date.now } }],
    flagcount: Number,
    unlikeby: [{ user: String, date: { type: Date, default: Date.now } }],
    unlikecount: Number,
    comments: [{ commentedBy: String, body: String, date: { type: Date, default: Date.now } }],
    commentcount: Number,
    createdOn: { type: Date, default: Date.now },
    hidden: { type: Boolean, default: false },
    category: String,
    featured:{ type: Boolean, default: false }
});

var model = mongoose.model('blog', blogSchema); 

exports.save = function (postData, cb) {

    var data = new model({
        postedBy    : postData.postedBy,
        title       : postData.title,
        author      : postData.author,
        body        : postData.body,
        imageUrl    : postData.imageUrl,
        creatorImage: postData.creatorImageUrl,
        creatorName : postData.creatorName,      
        likecount   : 0,
        flagcount   : 0,
        unlikecount : 0,
        commentcount: 0,
        hidden      : postData.hidden,
        category    : postData.category,
        type        : postData.type,
        featured    : postData.featured
     });
    
    
    data.save(function (err,doc) {
          if(err)
             cb(err,null);
          else{
               cb(null,doc);
            }  
       });
       
    // db.insert(data, function (err) {
    //     if (err)
    //         cb(err);
    //     else
    //         cb(null);
    // })
}

exports.model = model;