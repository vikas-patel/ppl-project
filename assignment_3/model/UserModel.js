var db = require('../model/MongoDbFactory.js');
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var crypt = require('../public/javascripts/crypto.js');

var UserSchema = new Schema({
    userName         : String,
    password         : String,
    email            : String,
    firstName        : String,
    lastName         : String,
    profilePic       : String,
    album            : [{ imageName: String, date: { type: Date, default: Date.now } }],
    verification_code: String,
    verified         : Boolean,
    token            : String,
    reset_pass_token : String,
    role             : String,
    date             : { type: Date, default: Date.now }
});

var Role = new Schema({
    name :String,
    actions : []  //permissions    
});

var user = mongoose.model('users', UserSchema);

exports.save = function(userdata,code,callback) {
    console.log(userdata);
    var data = new user({
                         userName:userdata.useName,
                         password: crypt.encrypt(userdata.password),
                         email: userdata.email,
                         firstName: userdata.firstName,
                         lastName: userdata.lastName,
                         profilePic: null,
                         token:userdata.token,
                         verification_code: code,
                         verified: false,
                         reset_pass_token: null,
                         role: "user",
                        });
                        
     db.insert(data,function(err) {
                   if (err)
                       callback(err);
                   else
                       callback(null);
            })
        }
        
        
exports.isEmailExist = function (email,next) {
    db.select({ "email": email }, user, function (err, result) {
        if(err){
         next(err,false);  //return error  
        }
        else{
            try {
                  result[0].UserID;
                   next(err,true);//return true because user found
                         
                 } catch (error) {
                      next(err,false);//return false because user not found
                 }
            }
    })
}

module.exports.model = user;
