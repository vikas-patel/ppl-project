var HttpStatus = require('http-status-codes');
var validator = require('validator');
var uid = require('uid-safe');
var usermodel = require('../model/UserModel.js');
var db = require('../model/MongoDbFactory.js');
var crypt = require('../public/javascripts/crypto.js');
var mailer = require('../model/nodemailer.js'); 

var StatusObj = {
    data: null,
    ResponseMessage: null
}


//login module
exports.login = function (req, res) {
    console.log(req.body)
try{
    //check for valid mail
    if (validator.isEmail(req.body.email)) {
        usermodel.isEmailExist(req.body.email, function (err, result) { //if not found means user not registered
            if (err) {
                StatusObj.data = false;
                StatusObj.ResponseMessage = "server_error";

            } else {
                if (result) {//means email is in database(user is registered)
                    db.select({ 'email': req.body.email, 'password': crypt.encrypt(req.body.password) },
                        usermodel.model, function (err, result) {
                            if (err) {
                                StatusObj.data = false;
                                StatusObj.ResponseMessage = "error";

                                res.status(HttpStatus.INTERNAL_SERVER_ERROR);
                                res.send(StatusObj);
                            }
                            else {
                                try {
                                    if (result[0].verified) {
                                        //if there is no data in result the exception will be thrown
                                        // if code come here the user found
                                        // and verified  
                                        var dataToBeSent = {
                                            _id:result[0]._id,
                                            userName: result[0].userName,
                                            email: result[0].email,
                                            firstName: result[0].firstName,
                                            lastName: result[0].lastName,
                                            role: result[0].role,
                                            date: result[0].date
                                        };

                                        var token = getToken(18);
                                        
                                        db.update({ 'email': req.body.email },
                                            usermodel.model, { 'token': crypt.encrypt(token) }, function (err, result) {
                                                if (err) {
                                                    StatusObj.data = false;
                                                    StatusObj.ResponseMessage = "token_update_error";

                                                    res.status(HttpStatus.INTERNAL_SERVER_ERROR);
                                                    res.send(StatusObj);
                                                }
                                                else {
                                                    res.setHeader('Authorization', "Bearer " + token);
                                                    StatusObj.data = dataToBeSent;
                                                    StatusObj.ResponseMessage = "success";
                                                    
                                                    res.status(HttpStatus.OK);
                                                    res.send(StatusObj);
                                                }
                                            });
                                    }
                                    else {
                                        res.status(HttpStatus.UNAUTHORIZED);
                                        StatusObj.ResponseMessage = "!verified";
                                        res.send(StatusObj);
                                    }
                                } catch (error) {
                                    //no user is in databse with this email and password
                                    console.log(error);

                                    StatusObj.data = false;
                                    StatusObj.ResponseMessage = "incorect_Password";

                                    res.status(HttpStatus.NOT_FOUND);
                                    res.send(StatusObj);
                                }
                            }
                        });
                }
                else {
                    StatusObj.data = false;
                    StatusObj.ResponseMessage = "email_!found";

                    res.status(HttpStatus.NOT_FOUND);
                    res.send(StatusObj);
                }
            }
        })
    }
    else {
        StatusObj.data = false;
        StatusObj.ResponseMessage = "!valid_email";

        res.status(HttpStatus.BAD_REQUEST);
        res.send(StatusObj);
    }
 }catch(error){
    console.log(error)
    StatusObj.data = false;
    StatusObj.ResponseMessage = "!noEmailInBody";

    res.status(HttpStatus.BAD_REQUEST);
    res.send(StatusObj);
  }
}


exports.register = function (req, res) {
    if (validator.isEmail(req.body.email)) {

        usermodel.isEmailExist(req.body.email, function (err, result) {

            if (result) {
                StatusObj.data = false;
                StatusObj.ResponseMessage = "email already exist";

                res.status(HttpStatus.CONFLICT);
                res.send(StatusObj);
            }
            else {
                var code = getToken(5);
                
                usermodel.save(req.body,code, function (err) {
                    if (!err) {
                        StatusObj.data = true;
                        StatusObj.ResponseMessage = "user saved";
                        
                        mailer.mailOptions.subject = 'verification code';
                        mailer.mailOptions.text = 'verify your account here';
                        mailer.mailOptions.to = req.body.email;
                        mailer.mailOptions.html = "<a href='http://127.0.0.1:3000/public/verifyuser/email/verification_code/"+code+"'>Click Here</a>"
                       
                        mailer.send(mailer.mailOptions);
                        
                        res.status(HttpStatus.OK);
                        res.send(StatusObj);
                    }
                    else {
                        StatusObj.data = false;
                        StatusObj.ResponseMessage = "server error";

                        res.status(HttpStatus.INTERNAL_SERVER_ERROR);
                        res.send(StatusObj);
                    }
                });
            }
        })
    } else {
        StatusObj.data = false;
        StatusObj.ResponseMessage = "not a valid email";

        res.status(HttpStatus.BAD_REQUEST);
        res.send(StatusObj);
    }
}

exports.logout = function (req,res) {
    console.log(req.headers['authorization']);
    var token = req.headers['authorization'].split(" ");
     db.update({ 'token': crypt.encrypt( token[1]) }, usermodel.model,
        { 'token': 'invalidatetokenwithdummytoken' }, function (err, result) {
            if (err) {
                StatusObj.ResponseMessage = "error ocured";

                res.status(HttpStatus.INTERNAL_SERVER_ERROR);
                res.send(StatusObj);
            } else {
                if (result) {
                    StatusObj.ResponseMessage = "success";

                    res.status(HttpStatus.OK);
                    res.send(StatusObj);
                }
                else {
                    StatusObj.ResponseMessage = 'error';

                    res.status(HttpStatus.OK);
                    res.send(StatusObj);
                }
            }
        })
}

exports.varifyUser = function (req, res) {
    db.update({ 'verification_code': req.params.code }, usermodel.model,
        { 'verified': true }, function (err, result) {
            if (err) {
                StatusObj.ResponseMessage = "error ocured";

                res.status(HttpStatus.INTERNAL_SERVER_ERROR);
                res.send(StatusObj);
            } else {
                if (result) {
                    StatusObj.ResponseMessage = "success";

                    res.status(HttpStatus.OK);
                    res.render('index', { title: 'Hey', message: 'Hello there!'});
                }
                else {
                    StatusObj.ResponseMessage = "wrong verification code";

                    res.status(HttpStatus.OK);
                    res.send(StatusObj);
                }
            }
        })
}

//when user click on forgot password link on website
exports.forgotPassword = function (req, res) {
    var token = getToken(18);
    db.update({ 'email': req.params.email }, usermodel.model,
        { 'reset_pass_token': token }, function (err, result) {
            if (err) {
                StatusObj.ResponseMessage = "error ocured";

                res.status(HttpStatus.INTERNAL_SERVER_ERROR);
                res.send(StatusObj);
            } else {
                if (result) {
                    StatusObj.ResponseMessage = "success";

                    mailer.mailOptions.subject = 'Forgot password';
                    mailer.mailOptions.text = 'Click on link to change your password';
                    mailer.mailOptions.to = req.params.email;
                    mailer.mailOptions.html = "Click on link to change your password<a href='http://127.0.0.1:3000/public/resetpassword/" + token + "'>Click Here</a>"

                    mailer.send(mailer.mailOptions);

                    console.log("done");
                    res.status(HttpStatus.OK);
                    res.send(StatusObj);
                }
                else {
                    StatusObj.ResponseMessage = "email not found";

                    res.status(HttpStatus.NOT_FOUND);
                    res.send(StatusObj);
                    return;
                }
            }
        });
}

//when user click on reset password link in mail
exports.verifyResetPassToken = function (req,res) {
    var token = getToken(18);
     db.update({ 'reset_pass_token': req.params.token }
                  ,usermodel.model,{'token':crypt.encrypt(token)},function (err,result) {
         if(err){
             StatusObj.ResponseMessage = "error ocured";

             res.status(HttpStatus.INTERNAL_SERVER_ERROR);
             res.send(StatusObj);
         }
         else{
             try {
                StatusObj.data = { "email" : result.email }
                StatusObj.ResponseMessage = "token matched";
                //user varyfied redirect to reset password page
                
                res.setHeader('Authorization', "Bearer " + token);
                var createtoken = '"'+token+'"'
                res.render('resetPass', { title: 'Hey', message: 'Hello there!',usertoken:createtoken});
                
                // res.status(HttpStatus.OK);
                // res.send(StatusObj);
             } catch (error) {
                 console.log(error);
                 StatusObj.ResponseMessage = "token not matched";
                
                res.status(HttpStatus.NOT_FOUND);
                res.send(StatusObj);
             }
         }
     })
}

//when user fill password and confirm password in page and click submit (change password)
exports.resetPassword = function (req, res) {
     
    db.update({ 'token': req.user.token}, usermodel.model,
        { 'password': crypt.encrypt( req.body.pass ) }, function (err, result) {
             if (err) {
                StatusObj.ResponseMessage = "error ocured";

                res.status(HttpStatus.INTERNAL_SERVER_ERROR);
                res.send(StatusObj);
            }else {
            if (result) {
                console.log(result);
                StatusObj.ResponseMessage = "success";
                 //redirect to login page
                res.status(HttpStatus.OK);
                res.send(StatusObj);
            }
            else {
                StatusObj.ResponseMessage = "invalid_token";

                res.status(401);
                res.send(StatusObj);
            }
           }
        });
}


exports.isEmailIdExists = function (req, res) {
    if (validator.isEmail(req.params.email)) {
        usermodel.isEmailExist(req.params.email, function (err, result) {
            if (err) {
                StatusObj.data = false;
                StatusObj.ResponseMessage = "server_error";

            } else {
                if (result) {
                    StatusObj.data = true;
                    StatusObj.ResponseMessage = "found";

                    res.status(HttpStatus.OK);
                    res.send(StatusObj);
                }
                else {
                    StatusObj.data = false;
                    StatusObj.ResponseMessage = "!found";

                    res.status(HttpStatus.NOT_FOUND);
                    res.send(StatusObj);
                }
            }
        })
    }

    else {
        StatusObj.data = false;
        StatusObj.ResponseMessage = "not a valid email";

        res.status(HttpStatus.BAD_REQUEST);
        res.send(StatusObj);
    }
}

var getToken = function (length) {
    return uid.sync(length);
}