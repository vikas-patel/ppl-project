var usermodel = require('../model/UserModel.js').model;
var passport = require('passport');
var Strategy = require('passport-http-bearer').Strategy;
var db = require('../model/MongoDbFactory.js');
var crypt = require('../public/javascripts/crypto.js');

	passport.serializeUser(function(user, done){
		done(null, user.id);
	});

	passport.deserializeUser(function(id, done){
        console.log("deserializeUser")
		usermodel.find({'_id':id}, function(err, user){
			done(err, user);
		});
	});

// Configure the Bearer strategy for use by Passport.
//
// The Bearer strategy requires a `verify` function which receives the
// credentials (`token`) contained in the request.  The function must invoke
// `cb` with a user object, which will be set at `req.user` in route handlers
// after authentication.
 passport.use(new Strategy({
    passReqToCallback: true
  },
  function(req,token, cb) {
    db.select({'token': crypt.encrypt(token)},usermodel, function(err, user) {
       if(err){
         cb(err);  //return error  
        }
        else{
            try {
                   user[0].UserID;
                   req.res.setHeader('Authorization', "Bearer " + token);//to send token to every response if valid auth token
                   cb(null,user[0]);//return true because user found                       
                 } catch (error) {
                     cb(null,false);//return false because user not found
                 }
            }
      });
  }));
  
module.exports = passport;