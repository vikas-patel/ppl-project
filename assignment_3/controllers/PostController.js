var HttpStatus = require('http-status-codes');
var validator = require('validator');
var blogpost = require('../model/BlogPostModel.js');
var db = require('../model/MongoDbFactory.js');
var fs = require('fs');
var uid = require('uid-safe');
var usermodel = require('../model/UserModel.js');
var base64 = require('node-base64-image');
var nodemailer = require('../model/nodemailer.js');
 const net = require("net");
 
var StatusObj = {
    data: null,
    ResponseMessage: null
}

exports.editPost = function (req, res) {
    db.delete({ "_id": req.body.postId }, blogpost.model, function (err, result) {
        if (err) {
            StatusObj.ResponseMessage = "error while ocured deleting";
            res.json(StatusObj)  //return error  
        }
        else {
            try {
                console.log(result)
                result.title;
                savepost(req, res);
                  
            } catch (error) {
                StatusObj.data = null;
                StatusObj.ResponseMessage = "failure";
                
                res.status(HttpStatus.INTERNAL_SERVER_ERROR);
                res.json(StatusObj);
            }
        }
    });    
}

exports.delete = function (req, res) {
    db.delete({ "_id": req.params.postId }, blogpost.model, function (err, result) {
        if (err) {
            StatusObj.ResponseMessage = "error ocured";
            res.json(StatusObj)  //return error  
        }
        else {
            try {
                result.title;

                StatusObj.ResponseMessage = "success";
                res.json(StatusObj);
            } catch (error) {
                StatusObj.data = null;
                StatusObj.ResponseMessage = "failure";
                res.json(StatusObj);
            }
        }
    });
}


//array of names of images will be sent to user
exports.getAllProfilePics = function (req, res) {
    db.select({ 'token': req.user.token }, usermodel.model, function (err, result) {
        if (err) {
            StatusObj.ResponseMessage = "error ocured";
            res.json(StatusObj)  //return error  
        }
        else {
            try {
                result[0].token;
                StatusObj.data = result[0].album;
                
                StatusObj.ResponseMessage = "success";
                res.json(StatusObj);
            } catch (error) {
                StatusObj.data = null;
                StatusObj.ResponseMessage = "nopics";
                res.json(StatusObj);
            }
        }
    });
}

exports.getsingleProfilePic = function (req, res) {
    res.sendFile(req.params.imageName, { root: './uploads' });
}

exports.getImageOfPost = function (req, res) {

       res.sendFile(req.params.imageName, { root: './uploads' });
}

exports.getCreatorProfilePic = function (req, res) {

    res.sendFile(req.params.creatorImageUrl, { root: './uploads' });
}

exports.getProfilePic = function (req, res) {
    console.log(req.user.profilePic);

    var path = './uploads/' + req.user.profilePic,
        options = { localFile: true, string: true };
      
    console.log(path);

    base64.base64encoder(path, options, function (err, image) {
        if (err) { console.log(err); }
        res.send(image);
    }); 

    // res.sendFile(req.user.profilePic, { root: './uploads' });
}

exports.profilePicUpload = function (req, res) {
    var imageData = [];

    req.on('data', function (chunk) {
        imageData.push(chunk);
        console.log("Received body data:");
        console.log(chunk);
    });

    req.on('end', function () {

        buffer = Buffer.concat(imageData);

        var imageName = 'image' + uid.sync(7);
        var imagePath = './uploads/' + imageName;
        fs.writeFile(imagePath, buffer, function (err) {

            if (err) {
                StatusObj.ResponseMessage = "error ocured";
                res.json(StatusObj)  //return error  
            }
            else {
                var token = req.user.token;
                db.update({ 'token': token }, usermodel.model
                    , { 'profilePic': imageName }, function (err, result) {
                        if (err) {
                            StatusObj.data = false;
                            StatusObj.ResponseMessage = "error";
                            
                            res.status(HttpStatus.INTERNAL_SERVER_ERROR);
                            res.send(StatusObj);
                        }
                        else {
                            console.log("update profilepic", result);
                            StatusObj.data = result.email;
                            StatusObj.ResponseMessage = "success";

                            saveToAlbum(req, res, imageName);
                        }
                  });
            }
        });
    });
}

function saveToAlbum(req, res, imageName) {

    var conditions = {
        _id: req.user._id,
        //'album.imageName': { $ne: req.body.comment }
    };
    var update = {
        $addToSet: { album: { "imageName": imageName } }
    }

    db.deepSave(conditions, usermodel.model, update, function (err, result) {
        if (err) {
            StatusObj.data = false;
            StatusObj.ResponseMessage = "error";

            console.log(err);
            res.status(HttpStatus.INTERNAL_SERVER_ERROR);
            res.send(StatusObj);
        } else {
           if(result == null){
              StatusObj.data = false;
              StatusObj.ResponseMessage = "usernotfound";
            
              res.status(HttpStatus.NOT_FOUND);
              res.send(StatusObj);
           }
           else{
              StatusObj.data = imageName;
              StatusObj.ResponseMessage = "success";
            
              res.status(HttpStatus.OK);
              res.send(StatusObj);
           }
        }
    })
}

exports.getPostsOfUser = function (req, res) {
    db.select({'postedBy':req.user._id}, blogpost.model, function (err, result) {
        if (err) {
            StatusObj.ResponseMessage = "error ocured";
            res.json(StatusObj)  //return error  
        }
        else {
            try {
                result[0].title;
                StatusObj.data = result;
                StatusObj.ResponseMessage = "success";
                res.json(StatusObj);
            } catch (error) {
                StatusObj.data = null;
                StatusObj.ResponseMessage = "noposts";
                res.json(StatusObj);
            }
        }
    });
}

exports.getFlagedPosts = function (req, res) {
    db.select({ 'flagcount': { $ne: 0}}, blogpost.model, function (err, result) {
        if (err) {
            StatusObj.ResponseMessage = "error ocured";
            res.json(StatusObj)  //return error  
        }
        else {
            try {
                result[0].title;
                sendResponseByIndex(req.params.index,res,result);
            } catch (error) {
                StatusObj.data = null;
                StatusObj.ResponseMessage = "noposts";
                res.json(StatusObj);
            }
        }
    });
}

exports.getOlderPostsFirst = function (req,res) {
    db.sort(blogpost.model,{},{"createdOn": 1},function (err ,result) {
        if (err) {
            StatusObj.ResponseMessage = "error ocured";
            res.json(StatusObj)  //return error  
        }
        else {
            try {
                result[0].title;
                sendResponseByIndex(req.params.index,res,result);
            } catch (error) {
                StatusObj.data = null;
                StatusObj.ResponseMessage = "noposts";
                res.json(StatusObj);
            }
        }
    });
}

exports.getLatestPostsFirst = function (req,res) {
    db.sort(blogpost.model,{},{"createdOn": -1},function (err ,result) {
          if (err) {
            StatusObj.ResponseMessage = "error ocured";
            res.json(StatusObj)  //return error  
        }
        else {
            try {
                result[0].title;
                sendResponseByIndex(req.params.index,res,result);
            } catch (error) {
                StatusObj.data = null;
                StatusObj.ResponseMessage = "noposts";
                res.json(StatusObj);
            }
        }
    })
}

exports.getPostsByFeatured = function (req, res) {
    var params = req.params.category_index;
     var values = params.split("_");
     console.log(values[0],values[1]);
    
    db.select({ 'category': values[0], 'featured': true}, blogpost.model, function (err, result) {
        if (err) {
            StatusObj.ResponseMessage = "error ocured";
            res.json(StatusObj)  //return error  
        }
        else {
            try {
                result[0].title;
                sendResponseByIndex(values[1], res, result);
            } catch (error) {
                StatusObj.data = null;
                StatusObj.ResponseMessage = "noposts";
                res.json(StatusObj);
            }
        }
    });
}

exports.getPostsByCategory = function (req, res) {
     var params = req.params.category_index;
     var values = params.split("_");
             console.log(values[0],values[1]);
    db.select({'category':values[0]}, blogpost.model, function (err, result) {
        if (err) {
            StatusObj.ResponseMessage = "error ocured";
            res.json(StatusObj)  //return error  
        }
        else {
            try {
                result[0].title;
                sendResponseByIndex(values[1], res, result);
            } catch (error) {
                StatusObj.data=null;
                StatusObj.ResponseMessage = "noposts";
                res.json(StatusObj);
            }
        }
    });
}

exports.getPosts = function (req, res) {
    db.select({}, blogpost.model, function (err, result) {
        if (err) {
            StatusObj.ResponseMessage = "error ocured";
            res.json(StatusObj)  //return error  
        }
        else {
            try {
                result[0].title;
                sendResponseByIndex(req.params.index,res,result);
            } catch (error) {
                StatusObj.ResponseMessage = "noposts";
                StatusObj.maxposts = result.length;
                res.status(HttpStatus.NOT_FOUND);
                res.json(StatusObj);
            }
        }
    });
}

function sendResponseByIndex(startIndex, res, result) {
    populate(startIndex, result)
        .then(function (finalResult) {
            //console.log("Final result " + finalResult);

            StatusObj.ResponseMessage = "success";
            StatusObj.data = finalResult;
            
            StatusObj.maxposts = result.length;
            
            res.status(HttpStatus.OK);
            res.send(StatusObj);
        })
        .catch(function (e) {
            console.log("Catch handler " + e);
            StatusObj.ResponseMessage = "failure";
            
            StatusObj.maxposts = result.length;
            
            res.status(HttpStatus.INTERNAL_SERVER_ERROR);
            res.send(StatusObj);
        })
}

function populate(startIndex, docs) {
    console.log("length of doc" + docs.length, startIndex + "<" + docs.length)
    return new Promise(function (resolve, reject) {
        var posts = [];
        var limit =parseFloat( startIndex ) + 3;

        for (var index = startIndex; index < docs.length && index < limit; index++) {
            posts.push(docs[index]);
            console.log("index"+docs.length && index +"<"+ limit)
        }
       // console.log(posts);
        resolve(posts);
    });
}

var savepost = function (req, res) {
    console.log(req.body.title);
    db.select({ 'title': (req.body.title).trim() }, blogpost.model, function (err, result) {
        if (err) {
            StatusObj.data = false;
            StatusObj.ResponseMessage = "error";

            res.status(HttpStatus.INTERNAL_SERVER_ERROR);
            res.send(StatusObj);
        }
        else {
            try {
                result[0].title;
                StatusObj.data = false;
                StatusObj.ResponseMessage = "already_exist";

                res.status(HttpStatus.NOT_FOUND);
                res.send(StatusObj);
            }
            catch (error) {

                console.log(error);

                var bodydata = req.body;
                    
                     req.body.postedBy = req.user._id;
                     req.body.author =  req.user.firstName +  " " + req.user.lastName;
                     req.body.creatorImageUrl = req.user.profilePic;
                     req.body.creatorName =  req.user.firstName +  " " + req.user.lastName;
                     
                    console.log(bodydata);

                    blogpost.save(req.body, function (err, doc) {
                        if (!err) {
                            StatusObj.data = doc._id;
                            StatusObj.ResponseMessage = "success";

                            res.status(HttpStatus.OK);
                            res.send(StatusObj);
                        }
                        else {
                            StatusObj.data = false;
                            StatusObj.ResponseMessage = "server_error";
                            console.log(err);
                            res.status(HttpStatus.INTERNAL_SERVER_ERROR);
                            res.send(StatusObj);
                        }
                    });
            }
        }
    })
}
exports.savePost = savepost;

exports.savePostImage = function (req, res) {
    var postId = req.headers['id'];

    saveImage(req, res, function (imageName) {
        db.update({ '_id': postId }, blogpost.model, { 'imageUrl': imageName }, function (err, result) {
            if (err) {
                StatusObj.data = false;
                StatusObj.ResponseMessage = "error";

                res.status(HttpStatus.INTERNAL_SERVER_ERROR); 
                res.send(StatusObj);
            }
            else {
                StatusObj.data = result.imageUrl;
                StatusObj.ResponseMessage = "success";
               
                res.status(HttpStatus.OK); 
                res.send(StatusObj);
            }
        })
    })
}


exports.saveImage = saveImage;

function saveImage(req,res,callback) {
    var imageData = [];

    req.on('data', function (chunk) {
        imageData.push(chunk);
        console.log("Received body data:");
        console.log(chunk);
    });

    req.on('end', function () {

        buffer = Buffer.concat(imageData);

        var imageName = 'image' + uid.sync(7);
        var imagePath = './uploads/' + imageName;
        fs.writeFile(imagePath, buffer, function (err) {

            if (err) {
                StatusObj.ResponseMessage = "error ocured";
                res.json(StatusObj)  //return error  
            }
            else {
               callback(imageName);
            }
        });
    });
}


exports.getSinglePost = function (req, res) {
     db.select({ _id:req.params.postid }, blogpost.model, function (err, result) {
        if (err) {
            StatusObj.data = false;
            StatusObj.ResponseMessage = "error";

            res.status(HttpStatus.INTERNAL_SERVER_ERROR);
            res.send(StatusObj);
        }
        else {
            try {
                result[0].title;
                StatusObj.data = result;
                StatusObj.ResponseMessage = "detail of " + req.params.postid;

                res.status(HttpStatus.OK);
                res.send(StatusObj);
            }
            catch (error) {
                //no post with this id
                console.log(error);
                
                res.status(HttpStatus.NOT_FOUND);
                StatusObj.ResponseMessage = "not found";
                res.send(StatusObj);    
            }
        }
    })
}

exports.saveComments = function (req, res) {
    //req.user.email this come from passport if user have valid token
    var conditions = {
         _id:req.body._id,
        'comments.body': { $ne: req.body.comment }
    };
    var update = {
        $addToSet: { comments: { "commentedBy": req.user.email, "body": req.body.comment, "date": new Date() } }
    }
    deepsave(conditions, update, req, res,{_id:req.body._id , field:'commentcount'});
}

exports.saveLike = function (req, res) {
    //_id is post id
    var conditions = {
        _id:req.params.postId,
        'likeby.user': { $ne: req.user.email }
    };
    var update = {
        $addToSet: { likeby: { "user": req.user.email, "date": new Date() } }
    } 
    deepsave(conditions, update, req , res, {_id:req.params.postId , field:'likecount'});
    
}

exports.saveflagBy = function (req, res) {
    //_id is post id
    var conditions = {
        _id:req.params.postId,
        'flagby.user': { $ne: req.user.email }
    };

    var update = {
        $addToSet: { flagby: { "user": req.user.email, "date": new Date() } }
    }   
    deepsave(conditions, update, req, res , {_id:req.params.postId , field:'flagcount'});
}  
  
exports.saveUnlike = function (req, res) {
     //_id is post id
    var conditions = {
        _id:req.params.postId,
        'unlikeby.user': { $ne: req.user.email }
    };

    var update = {
        $addToSet: { unlikeby: { "user": req.user.email, "date": new Date() } }
    }   
    deepsave(conditions, update, req, res , {_id:req.params.postId , field:'unlikecount'});
}


function deepsave(conditions, update, req, res, count) {
    db.deepSave(conditions, blogpost.model, update, function (err, doc) {
        if (err) {
            StatusObj.data = false;
            StatusObj.ResponseMessage = "error";

            console.log(err);
            res.status(HttpStatus.INTERNAL_SERVER_ERROR);
            res.send(StatusObj);
        } else {
            if (doc == null) {

                if (count.field == 'commentcount') { //if comment already saved 
                    StatusObj.data = true;
                    StatusObj.ResponseMessage = "alreadySaved";

                    res.status(HttpStatus.CONFLICT);
                    res.send(StatusObj);
                }
                else {//do decrement for like/unlike/flag if already saved
                    console.log(Object.keys(conditions)[1].split('.')[0]);

                    var fieldName = Object.keys(conditions)[1].split('.')[0];
                    
                   var field = JSON.parse('{"'+ fieldName+'": { "user":"'+ req.user.email+'" } }')               
                   
                    db.deepDelete(conditions._id, blogpost.model
                        , { $pull: field }
                        , function (err, doc) {
                            if (err) {
                                StatusObj.data = false;
                                StatusObj.ResponseMessage = "error";

                                console.log(err);
                                res.status(HttpStatus.INTERNAL_SERVER_ERROR);
                                res.send(StatusObj);
                            } else {
                                    decrementCount(count._id, count.field)
                                        .then(function (finalResult) {
                                            console.log("Final result " + finalResult)

                                            StatusObj.ResponseMessage = "success";
                                            StatusObj.data = finalResult;

                                            res.status(HttpStatus.OK);
                                            res.send(StatusObj);
                                        })
                                        .catch(function (e) {
                                            console.log("Catch handler " + e);
                                            StatusObj.ResponseMessage = "failure";

                                            res.status(HttpStatus.INTERNAL_SERVER_ERROR);
                                            res.send(StatusObj);
                                        })
                            }
                        })


                }
            }
            else {
                incrementCount(count._id, count.field)
                    .then(function (finalResult) {
                        console.log("Final result " + finalResult)

                        StatusObj.ResponseMessage = "success";
                        StatusObj.data = finalResult;

                        res.status(HttpStatus.OK);
                        res.send(StatusObj);
                    })
                    .catch(function (e) {
                        console.log("Catch handler " + e);
                        StatusObj.ResponseMessage = "failure";

                        res.status(HttpStatus.INTERNAL_SERVER_ERROR);
                        res.send(StatusObj);
                    });
            }
        }
    })
}


//likecount/unlike/commentcount/flagcount
function incrementCount(postId, columnName) {
    return new Promise(function (resolve, reject) {
        db.select({'_id' : postId}, blogpost.model, function (err, result) {
            if (err) {
                reject(err);
            }
            else {
                try {
                    var data = eval("result[0]." + columnName);
                    var update = null;

                    if (columnName == 'likecount'){
                        update = { 'likecount': data + 1 }}
                    if (columnName == 'unlike'){
                        update = { 'unlike': data + 1 }}
                    if (columnName == 'commentcount'){
                        update = { 'commentcount': data + 1 }}
                    if (columnName == 'flagcount'){
                        update = { 'flagcount': data + 1 }}
                    if (columnName == 'unlikecount'){
                        update = { 'unlikecount': data + 1 }}
                    if (columnName == 'profilePic'){
                        update = { 'profilePic': data + 1 }}
                        

                    db.update({ '_id': postId }, blogpost.model,
                        update, function (err, result) {
                            if (err) {
                                reject(err);
                            } else {
                                if (result) {
                                    resolve(result);
                                }
                                else {
                                    reject("column_not_found");
                                }
                            }
                        });
                } catch (err) {
                    reject(err);
                }
            }
        });
    });
}


//likecount/unlike/commentcount/flagcount
function decrementCount(postId,columnName) {
    return new Promise(function (resolve, reject) {
        db.select({'_id' : postId}, blogpost.model, function (err, result) {
            if (err) {
                reject(err);
            }
            else {
                try {
                    var data = eval("result[0]." + columnName);
                    var update = null;

                    if (columnName == 'likecount'){
                        update = { 'likecount': data - 1 }}
                    if (columnName == 'unlike'){
                        update = { 'unlike': data - 1 }}
                    if (columnName == 'commentcount'){
                        update = { 'commentcount': data - 1 }}
                    if (columnName == 'flagcount'){
                        update = { 'flagcount': data - 1 }}
                    if (columnName == 'unlikecount'){
                        update = { 'unlikecount': data - 1 }}

                    db.update({ '_id': postId }, blogpost.model,
                        update, function (err, result) {
                            if (err) {
                                reject(err);
                            } else {
                                if (result) {
                                    resolve(result);
                                }
                                else {
                                    reject("column_not_found");
                                }
                            }
                        });
                } catch (err) {
                    reject(err);
                }
            }
        });
    });
}