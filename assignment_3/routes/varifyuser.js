var express = require('express');
var router = express.Router();
var passport =  require('../controllers/passport');
var usercontroller = require('../controllers/UserController');

var varifyuser = require('./varifyuser');
 
router.get('/email/verification_code/:code',usercontroller.varifyUser); //Verify user by email

module.exports = router;