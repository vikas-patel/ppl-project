var express = require('express');
var router = express.Router();
var passport =  require('../controllers/passport');
var usercontroller = require('../controllers/UserController');
var postController = require('../controllers/PostController');
var varifyuser = require('./varifyuser');
 
 router.use('/verifyuser',varifyuser);
 
 router.post('/register', usercontroller.register);
 router.post('/login', usercontroller.login);                          //Login user
 router.get('/forgotpassword/:email', usercontroller.forgotPassword);      //forget password by email
 router.get('/resetpassword/:token',usercontroller.verifyResetPassToken);  //recieve params from link and grant user to reset password by verifying token 
 router.get('/posts/:index',postController.getPosts);
 router.get('/post/:postid',postController.getSinglePost);
 router.get('/category/:category_index',postController.getPostsByCategory);//
 router.get('/featured/:category_index',postController.getPostsByFeatured);//
 router.get('/latestPostsFirst/:index',postController.getLatestPostsFirst);
 router.get('/olderPostsFirst/:index',postController.getOlderPostsFirst);
 router.get('/flagedposts/:index',postController.getFlagedPosts);
 router.get('/postPic/:imageName',postController.getImageOfPost);
 router.get('/creatorImage/:creatorImageUrl',postController.getCreatorProfilePic);
 router.get('/isEmailIdExist/:email', usercontroller.isEmailIdExists);
 router.get('/album/:imageName', postController.getsingleProfilePic);
 
 //router.get('/mail/:email',postController.mailservertest);
 
 
 /* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('in public');
});

module.exports = router;