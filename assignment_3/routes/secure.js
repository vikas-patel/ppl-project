var express = require('express');
var router = express.Router();
var passport = require('../controllers/passport');
var postController = require('../controllers/PostController');
var usercontroller = require('../controllers/UserController');

router.get('/', passport.authenticate('bearer', { session: false }), function (req, res) {
    res.send('authorized_route');
});

router.get('/logout',passport.authenticate('bearer', { session: false }),usercontroller.logout);
router.post('/savePost',passport.authenticate('bearer', { session: false }),postController.savePost);
router.post('/saveComments',passport.authenticate('bearer', { session: false }),postController.saveComments);
router.get('/post/like/:postId',passport.authenticate('bearer', { session: false }),postController.saveLike);
router.get('/post/unlike/:postId',passport.authenticate('bearer', { session: false }),postController.saveUnlike);
router.get('/post/flagby/:postId',passport.authenticate('bearer', { session: false }),postController.saveflagBy);
router.get('/post/getPostsOfUser',passport.authenticate('bearer', { session: false }),postController.getPostsOfUser);
router.post('/profilePicUpload',passport.authenticate('bearer', { session: false }),postController.profilePicUpload);
router.get('/profilePic',passport.authenticate('bearer', { session: false }),postController.getProfilePic);
router.post('/savePostImage',passport.authenticate('bearer', { session: false }),postController.savePostImage);
router.post('/resetpassword',passport.authenticate('bearer', { session: false }),usercontroller.resetPassword);
router.get('/album',passport.authenticate('bearer', { session: false }), postController.getAllProfilePics);
router.get('/delete/:postId',passport.authenticate('bearer', { session: false }), postController.delete);
router.post('/editPost',passport.authenticate('bearer', { session: false }), postController.editPost);

module.exports = router;