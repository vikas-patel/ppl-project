var express = require('express');
var router = express.Router();
var _public = require('./public');
var secureRoute = require('./secure');
var usercontroller = require('../controllers/UserController');
var HttpStatus = require('http-status-codes');

router.use('/public', _public);
router.use('/secure',secureRoute);

/* GET home page. */
router.get('/', function(req, res, next) {
    res.status(HttpStatus.OK);
  res.render('index', { title: 'login Authentication Api using get' });
});

module.exports = router;
