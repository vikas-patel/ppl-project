var pplapp = angular.module('pplapp', ['ui.router','ngFileUpload','ngSanitize']);

pplapp.factory('localstorage', function ($rootScope,$window) {
    var mem = {};
 
    return {
        put: function (key, value) {
            $window.localStorage.setItem(key, JSON.stringify(value));
        },
        get: function (key) {
            return JSON.parse($window.localStorage.getItem(key));
        }
    };
});

pplapp.service('userService', function ($http, $window, $state) {

    this.isUserLogedIn = function (callback) {

        if (getToken()) {
            console.log(getToken());
         //   setTimeout(function () {
                tokenAuthTest(getToken())
                    .then(function (finalResult) {
                        callback(null, finalResult);
                    })
                    .catch(function (e) {
                        callback(e, null);
                    });
           // }, 200);
        }
        else {
            callback("notLogedIn", false);
        }
    }

    function getToken() {
       return $window.sessionStorage.getItem('token');
    }
    
    function tokenAuthTest(token) {
        return new Promise(function (resolve, reject) {
        $http({
            url: 'http://127.0.0.1:3000/secure',
            method: 'GET',
            headers: { 'Authorization': token }
        }).
            success(function (response) {
                // Getting Success Response in Callback
                console.log("success tokenAuthTest");
              resolve(true);
            }).
            error(function (response) {
                // Getting Error Response in Callback             
                console.log(response+" tokenAuthTest")
                reject(false);
            });
       })
    }
})


// Reddit constructor function to encapsulate HTTP and pagination logic
pplapp.factory('Reddit', function ($http, $window) {
    var Reddit = function () {
        this.items = [];
        this.busy = false;
        this.after = 0;
        this.noOfPosts = -1;
    };
    Reddit.prototype.loadMore = function () {
        if (this.busy) return;
        this.busy = true;

        var url = $window.localStorage.getItem('url') + this.items.length;

        if (this.noOfPosts == this.items.length) {
            console.log('end',this.noOfPosts, this.items.length)
            this.busy = false;
        }
        else {
            console.log(this.noOfPosts, this.items.length);
            this.noOfPosts = this.items.length;
            $http({
                url: url,
                method: 'GET',
            }).
                success(function (response) {
                    // Getting Success Response in Callback              
                    response.data.forEach(function (element) {

                        this.items.push(element);

                    }, this);

                    this.after = this.items[this.items.length - 1];
                    this.busy = false;

                }.bind(this)).
                error(function (response) {
                    // Getting Error Response in Callback             
                    console.log(response);
                });
        }
    };
    return Reddit;
});

/*pplapp.run(function ($rootScope, $state,userService) {
    $rootScope.$on('$stateChangeStart',
        function (event, toState, toParams, fromState, fromParams) {
            if (!userService.checkIfUserLogedIn()) {
                event.preventDefault();
                console.log("im here");
                $state.go('login');
            }            
            console.log("here");
        });
});*/