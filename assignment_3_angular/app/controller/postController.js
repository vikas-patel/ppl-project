pplapp.controller('rootController', ['$scope', '$http', '$window', '$state', 'localstorage', 'userService','$rootScope',
    function ($scope, $http, $window, $state, localstorage, userService, $rootScope) {

        if ($state.current.name = 'home.timeline' && !getToken()) {
            $state.go('login');
        }
        
        $scope.changeState = function (state) {
            localstorage.put('featured', null);
            localstorage.put('category', null);
            $state.go(state);
        }

        function getToken() {
            return $window.sessionStorage.getItem('token');
        }

        $rootScope.$on( "permission", function () {
            if (getToken()) {
                $scope.user.permission = true;     
                var userobj = getCurrentUser();          
                $scope.username = userobj.firstName;  
                console.log("root controller token=" + getToken(),$scope.username);
            }
            else {
                $scope.user.permission = false;
                console.log("else of root controller");
            }
        })
        
        $rootScope.$emit("permission", {});
        
         function getCurrentUser() {
             var user = JSON.parse(sessionStorage.getItem('user'));
             return user;
         }
        
          $scope.detailPost = function (postid) {
            $http({
                url: 'http://127.0.0.1:3000/public/post/' + postid,
                method: 'GET',
              }).
                success(function (response) {
                    // Getting Success Response in Callback
                    localstorage.put('singlePost', response.data[0]);
                    $state.go('singlepost');
                }).
                error(function (response) {
                    // Getting Error Response in Callback             
                    $scope.response = response || "Request failed";
                    //show popup
             });      
        }
        
        $scope.showPosts = true;// by default posts will be shown on timeline
        
        $scope.showCreatePost = function () {
            $scope.createPost = true;
            $scope.showPosts = false;
            $state.go('home.timeline');
        }
        
        $scope.showTimelinePosts = function () {
            $scope.createPost = false;
            $scope.showPosts = true;
            $scope.showAlbum = false;
            $state.go('home.timeline');
         }
         
         $rootScope.$on("showHideAlbum", function(){
           $scope.showAlbum = true;
           $scope.createPost = false;
           $scope.showPosts = false;
           console.log("heereeeeee")
        });
        
    }]);

pplapp.controller('postsController', ['$scope', '$http', '$window', '$state', 'localstorage', 'userService', 'Reddit',
    function ($scope, $http, $window, $state, localstorage, userService, Reddit) {

        $scope.reddit = new Reddit();
        console.log($scope.reddit);

        if ($state.current.name == 'home.timeline') {
            $scope.showProfile = true;
        }
        else {
            $scope.showProfile = false;
        }
        
        //display posts for allposts,featured,category
        if (localstorage.get('featured') != null) {
            $window.localStorage.removeItem('url');
            $window.localStorage.setItem('url', 'http://127.0.0.1:3000/public/featured/' + localstorage.get('featured') + '_');
            localstorage.put('featured',null);
        }
        else {
            if (localstorage.get('category') != null) {
                $window.localStorage.removeItem('url');
                $window.localStorage.setItem('url', 'http://127.0.0.1:3000/public/category/' + localstorage.get('category') + '_');
                localstorage.put('category',null);
            }
            else {
                console.log('initial posts');
                $window.localStorage.removeItem('url');
                $window.localStorage.setItem('url', 'http://127.0.0.1:3000/public/posts/');
                $state.go('home');
            }
        }

        $scope.getLatestPostsFirst = function () {
            $window.localStorage.removeItem('url');
            $window.localStorage.setItem('url', 'http://127.0.0.1:3000/public/LatestPostsFirst/');

            loadPosts();
        }

        $scope.getOlderPostsFirst = function () {
            $window.localStorage.removeItem('url');
            $window.localStorage.setItem('url', 'http://127.0.0.1:3000/public/olderPostsFirst/');

            loadPosts();
        }

        $scope.changeFlag = function () {
            if ($scope.flag.change) {
                $window.localStorage.removeItem('url');
                $window.localStorage.setItem('url', 'http://127.0.0.1:3000/public/flagedposts/');

                loadPosts();
            }
            else {
                $window.localStorage.removeItem('url');
                $window.localStorage.setItem('url', 'http://127.0.0.1:3000/public/posts/');

                loadPosts();
            }
        }

        function loadPosts() {
            $scope.reddit.items = [];
            console.log($scope.reddit.items)
            $scope.reddit.noOfPosts = -1;
            $scope.reddit.loadMore();
        }

    }]);

pplapp.controller('singlePostController', ['$scope', '$http', '$window', '$state', 'localstorage',
    function ($scope, $http, $window, $state, localstorage) {

        $scope.singlePost = localstorage.get('singlePost');

        $scope.comments = [];

         viewMoreComments();
           
        console.log( $scope.singlePost.comments);
           
        $scope.flagby = function (postid) {
            $http({
                url: 'http://127.0.0.1:3000/secure/post/flagby/' + postid,
                method: 'GET',
                headers: { 'Authorization': $window.sessionStorage.getItem('token') }
            }).
                success(function (response) {
                    // Getting Success Response in Callback
                    localstorage.put('singlePost', response.data);
                    $scope.singlePost = response.data;
                    showDialogue("Done", "flagged");
                }).
                error(function (response) {
                    // Getting Error Response in Callback             
                    $scope.response = response || "Request failed";
                    showDialogue("Oops cant save your flag","login first");
                });
        }
        
        $scope.getFlaged = function () {
            var flagcount = localstorage.get('singlePost').flagcount;
            if (flagcount > 0) {
                console.log("in flaged function ="+flagcount);
                $scope.flaged.selected = true;
            }
            else {
                $scope.flaged.selected = false;
            }
        }

        $scope.likecount = function (postid) {
            $http({
                url: 'http://127.0.0.1:3000/secure/post/like/' + postid,
                method: 'GET',
                headers: { 'Authorization': $window.sessionStorage.getItem('token') }
            }).
                success(function (response) {
                    // Getting Success Response in Callback
                    localstorage.put('singlePost', response.data);
                    $scope.singlePost = response.data;
                }).
                error(function (response) {
                    // Getting Error Response in Callback             
                    $scope.response = response || "Request failed";
                    showDialogue("Oops cant save your like","login first");
                });    
        }

        $scope.unlike = function (postid) {
            $http({
                url: 'http://127.0.0.1:3000/secure/post/unlike/' + postid,
                method: 'GET',
                headers: { 'Authorization': $window.sessionStorage.getItem('token') }
            }).
                success(function (response) {
                    // Getting Success Response in Callback
                    localstorage.put('singlePost', response.data);
                    $scope.singlePost = response.data;
                }).
                error(function (response) {
                    // Getting Error Response in Callback             
                    $scope.response = response || "Request failed";
                    showDialogue("Oops cant save your like","login first");
                });
        }

        $scope.savecomment = function (postid) {
            $http({
                url: 'http://127.0.0.1:3000/secure/saveComments/',
                method: 'POST',
                data: { "_id": postid, "comment": $scope.comment },
                headers: { 'Authorization': $window.sessionStorage.getItem('token') }
            }).
                success(function (response) {
                    // Getting Success Response in Callback
                    localstorage.put('singlePost', response.data);
                    $scope.singlePost = response.data;
                    
                    $scope.comments = [];
                    viewMoreComments();
                     
                    $scope.comment = null;
                }).
                error(function (response) {
                    // Getting Error Response in Callback             
                    $scope.response = response || "Request failed";
                    console.log(response);
                    showDialogue("Oops", "already saved");
                });
        }
          
         $scope.viewMoreComments = viewMoreComments;
        
         function viewMoreComments() {
            var startIndex =  $scope.singlePost.comments.length - $scope.comments.length;

            for (var index = startIndex-1; index > startIndex - 4 && index >= 0; index--) {
                $scope.comments.push($scope.singlePost.comments[index]);
            }
        }
        
        function showDialogue(header, message) {
            $scope.pop.messageHeader = header;
            $scope.pop.mainMessage = message;
            $scope.pop.confirm = true;
        }
        
         $scope.hidedialogue = function () {
                $scope.pop.confirm = false;
            }
    }]);


pplapp.controller('rightController', ['$scope', '$http', '$window', '$state', 'localstorage', 'userService',
    function ($scope, $http, $window, $state, localstorage, userService) {    

        $scope.getPostsByFeatured = function (category) {
            console.log(category, $state.current);
            localstorage.put('featured', category);
            if ($state.current.name != 'home.category')
                $state.go('home.category');
            else
                $state.go('home.changecategory');
        }

        $scope.getPostBycategory = function (category) {
            console.log(category, $state.current);
            localstorage.put('category', category);
            if ($state.current.name != 'home.category')
                $state.go('home.category');
            else
                $state.go('home.changecategory');
        }
        
        //jquery
        $(document).ready(function () {
            $('#rght_cat_bg').click(function () {
                $('.rght_list').toggle("slide");
                console.log("hereeeeeeee");
            });
        });

        $(document).ready(function () {
            $('#opn_cat_bg').click(function () {
                $('.sub_dwn').toggle("slide");
            });
        });
        //end jquery
    }]);
    
pplapp.controller('timelineController', ['$scope', '$http', '$window', '$state', 'Upload', 'localstorage', 'userService','$rootScope',
    function ($scope, $http, $window, $state, Upload, localstorage, userService ,$rootScope) {
        $http({
            url: 'http://127.0.0.1:3000/secure/post/getPostsOfUser',
            method: 'GET',
            headers: { 'Authorization': $window.sessionStorage.getItem('token') }
        }).
            success(function (response) {
                // Getting Success Response in Callback
                localstorage.put('myPosts', response.data);
                $scope.posts = localstorage.get('myPosts');
                $scope.currentUser = JSON.parse(sessionStorage.getItem('user'));
                getProfilePic();
                console.log($scope.currentUser);
            }).
            error(function (response) {
                // Getting Error Response in Callback             
                $scope.response = response || "Request failed";
                console.log(response);
            });
            
        $scope.editPost = function (postid) {
            $http({
                url: 'http://127.0.0.1:3000/public/post/' + postid,
                method: 'GET',
            }).
                success(function (response) {
                    // Getting Success Response in Callback
                    $scope.newpost.title = response.data[0].title;
                    $scope.newpost.featured = response.data[0].featured;
                    $scope.newpost.body = response.data[0].body;
                    $scope.newpost.category = response.data[0].category;
                    $scope.newpost.type = response.data[0].type;
                    $scope.newpost.imageUrl = response.data[0].imageUrl;
                    $scope.newpost.postId = response.data[0]._id;
                    
                    $scope.editMode = true;
                    CKEDITOR.instances.editor.insertHtml(response.data[0].body);
                    
                    $scope.showCreatePost();
                    console.log($scope.newpost);
                }).
                error(function (response) {
                    // Getting Error Response in Callback             
                    $scope.response = response || "Request failed";
                    //show popup
                });
        }
     
        $scope.deletePost = function (posiId) {
            showDialogue("Confirmation","Do you really want to delete?");
            $http({
                url: 'http://127.0.0.1:3000/secure/delete/'+posiId,
                method: 'GET',
                headers: { 'Authorization': $window.sessionStorage.getItem('token') }
            }).
                success(function (response) {
                    // Getting Success Response in Callback
                    //$scope.posts = response.data;
                    showDialogue("deleted","post is seccessfully deleted refresh the window");
                }).
                error(function (response) {
                    // Getting Error Response in Callback             
                    $scope.response = response || "Request failed";
                    console.log(response);
                });
        }
            
        $scope.getAllProfilePics = function () {
            $http({
                url: 'http://127.0.0.1:3000/secure/album',
                method: 'GET',
                headers: { 'Authorization': $window.sessionStorage.getItem('token') }
            }).
                success(function (response) {
                    // Getting Success Response in Callback
                   $scope.album = response.data;
                   $rootScope.$emit("showHideAlbum", {});
                }).
                error(function (response) {
                    // Getting Error Response in Callback             
                    $scope.response = response || "Request failed";
                    console.log(response);
                });
        }
               
   var getProfilePic = function () {
            $http({
                url: 'http://127.0.0.1:3000/secure/profilepic',
                method: 'GET',
                headers: { 'Authorization': $window.sessionStorage.getItem('token') }
            }).
                success(function (response) {
                    // Getting Success Response in Callback
                    $scope.profilePic = response;
                    $rootScope.profilePic = $scope.profilePic;
                }).
                error(function (response) {
                    // Getting Error Response in Callback             
                    $scope.response = response || "Request failed";
                    console.log(response);
                });
        }

        $scope.changeProfilePic = function (file, errFiles) {
            console.log("print file",file);

            if (file) {
                Upload.http({
                    url: 'http://127.0.0.1:3000/secure/profilePicUpload',
                    data: file,
                    headers: {
                        'Content-Type': file.type,
                        'Authorization': $window.sessionStorage.getItem('token')
                    }
                }).then(function (resp) {
                    console.log(resp);
                    if (resp.status == 200) {
                        // $window.location.reload();
                        getProfilePic();
                    }

                });
            }
        }
        
        $scope.file;
        
        $scope.saveEditPost = function () {
          $scope.newpost.body = CKEDITOR.instances.editor.getData();
           $http({
                url: 'http://127.0.0.1:3000/secure/editPost',
                method: 'POST',
                data: $scope.newpost,
                headers: { 'Authorization': $window.sessionStorage.getItem('token'),
                           'Content-Type': 'application/json' }
            }).
                success(function (response) {
                    // Getting Success Response in Callback
                    $scope._id = response;
                    console.log(response);
                    
                    if($scope.file){
                       saveImage(response.data);
                    }
                    
                    //showDialogue("Success","Your Post is saved");
                    window.location.reload();              
                }).
                error(function (response) {
                    // Getting Error Response in Callback             
                    $scope.response = response || "Request failed";
                    showDialogue("Error",response.ResponseMessage); 
                    console.log(response);
                }); 
        }
        
        $scope.savePost = function () {
          
          $scope.newpost.body = CKEDITOR.instances.editor.getData();
          
          if($scope.file){
             $http({
                url: 'http://127.0.0.1:3000/secure/savePost',
                method: 'POST',
                data: $scope.newpost,
                headers: { 'Authorization': $window.sessionStorage.getItem('token'),
                           'Content-Type': 'application/json' }
            }).
                success(function (response) {
                    // Getting Success Response in Callback
                    $scope._id = response;
                    console.log(response);
         
                    saveImage(response.data);   
                    showDialogue("Success","Your Post is saved");              
                }).
                error(function (response) {
                    // Getting Error Response in Callback             
                    $scope.response = response || "Request failed";
                    showDialogue("Error",response.ResponseMessage) ; 
                    console.log(response);
                });
          }
          else{
              $scope.emageerror = true;
          }
        }

        function showDialogue(header, message) {
            $scope.pop.messageHeader = header;
            $scope.pop.mainMessage = message;
            $scope.pop.confirm = true;
        }

        $scope.hidedialogue = function () {
            $scope.pop.confirm = false;
        }
        
        
        function saveImage(postid) {
            
            console.log($scope.file);
            
              Upload.http({
                    url: 'http://127.0.0.1:3000/secure/savePostImage',
                    data: $scope.file,
                    headers: {
                        'Id':postid,
                        'Content-Type': $scope.file.type,
                        'Authorization': $window.sessionStorage.getItem('token')
                    }
                }).then(function (resp) {
                    console.log(resp);
                    if (resp.status == 200) { 
                        getProfilePic();
                    }

                }).catch(function (err) {
                    showDialogue('Error!',"Error accured wile saving the image please try again");
                })
        }
        
        
        $scope.drawCanvas = function (file, errFiles) {
            var queue = [];
            $scope.file = file;
            queue.push(file);
            $scope.queue = queue;
            $scope.emageerror = false;
        }
        
        $scope.openEditor = function () {
            $scope.editor.show = true;
        }
        
        $scope.closeEditor = function () {
            $scope.editor.show = false;
           //  var data = CKEDITOR.instances.editor.getData();
             
             $scope.postBodyData = "<b>your content is saved, feel free to edit it<b>"//data;
            
             $scope.newpost.showtextArea = false;         
        }

        $scope.getPostImage = function (imageName) {
          $http({
                url: 'http://127.0.0.1:3000/public/postPic/'+imageName,
                method: 'GET',
            }).
                success(function (response) {
                    // Getting Success Response in Callback
                    $scope.PostImage = response;
                    console.log(response);                        
                }).
                error(function (response) {
                    // Getting Error Response in Callback             
                    $scope.response = response || "Request failed";
                    console.log(response);
                });
        }
        
        CKEDITOR.replace('editor');

    }]);