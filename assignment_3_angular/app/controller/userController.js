pplapp.controller('registerController', ['$scope', '$http', function ($scope, $http) {
    $scope.save = function () {
        $scope.user.__proto__ = null;
        $http({
            url: 'http://127.0.0.1:3000/public/register',
            method: 'POST',
            data: $scope.user,
            headers: { 'Content-Type': 'application/json' }
        }).
            success(function (response) {
                // Getting Success Response in Callback
                console.log(response)
                $scope.codeStatus = response.data;
                $scope.user.confirm = true;
            }).
            error(function (response) {
                // Getting Error Response in Callback             
                $scope.codeStatus = response || "Request failed";
                alert($scope.codeStatus.ResponseMessage);
            });
        $scope.hidedialogue = function () {
            $scope.user.confirm = false;
        }
    };
}]);

pplapp.controller('forgotPasswordController', ['$scope', '$http', function ($scope, $http) {
    $scope.sendmail = function () {
        $scope.user.__proto__ = null;
        $http({
            url: 'http://127.0.0.1:3000/public/forgotpassword/' + $scope.user.email,
            method: 'GET',
            data: $scope.user,
            headers: { 'Content-Type': 'application/json' }
        }).
            success(function (response) {
                // Getting Success Response in Callback
                console.log(response)
                $scope.codeStatus = response.data;
                $scope.user.hdr = 'Email confirmation';
                $scope.user.body = 'An email is sent please click on link mentioned in mail',
                $scope.user.confirm = true;
            }).
            error(function (response) {
                // Getting Error Response in Callback             
                $scope.codeStatus = response || "Request failed";
                $scope.user.hdr = 'Error';
                $scope.user.body = response.ResponseMessage,
                alert($scope.codeStatus.ResponseMessage);
            });
        $scope.hidedialogue = function () {
            $scope.user.confirm = false;
        }
    };
}]);

pplapp.controller('resetPasswordController', ['$scope', '$http', '$state', function ($scope, $http, $state) {
   console.log(location.search)
  
   $scope.comparePassword = function () {
       if (!$scope.user.pass == $scope.user.cnfpass) {
            $scope.resetPass.errmsg = true;
       }
       else {
           $scope.resetPass.errmsg = false;
           console.log($scope.user.pass ,$scope.user.cnfpass)
       }
   }
    
    $scope.changepass = function () {
        var token;
        if(location.search == ""){
            token = sessionStorage.getItem('token');
            }
        else{
            token = "Bearer " + (location.search).split("=")[1];
        }
        
        $scope.user.__proto__ = null;
        $http({
            url: 'http://127.0.0.1:3000/secure/resetpassword',
            method: 'POST',
            data: $scope.user,
            headers: { 'Authorization':  token}
        }).
            success(function (response) {
                // Getting Success Response in Callback            
                $scope.codeStatus = response.data;
                showDialogue("Success!", "your password is now changed please click on login");
                sessionStorage.removeItem('token');
            }).
            error(function (response) {
                // Getting Error Response in Callback             
                $scope.codeStatus = response || "Request failed";
                showDialogue("Error", $scope.codeStatus.ResponseMessage);
            });
    };

    function showDialogue(header, message) {
        $scope.resetPass.messageHeader = header;
        $scope.resetPass.mainMessage = message;
        $scope.resetPass.confirm = true;
    }

    $scope.hidedialogue = function () {
        $scope.resetPass.confirm = false;
        location.search = '';
        $state.go('login');
    }  
}]);

pplapp.controller('loginController', ['$scope', '$http', '$state', '$window','$rootScope', function ($scope, $http, $state, $window,$rootScope) {

    $scope.login = function () {
        console.log($scope.log)
        if($scope.log.email && $scope.log.password){
        $http({
            url: 'http://127.0.0.1:3000/public/login',
            method: 'POST',
            data: { 'email': $scope.log.email, 'password': $scope.log.password },
            headers: { 'Content-Type': 'application/json' }
        }).
            success(function (response, status, headers) {
                // Getting Success Response in Callback
                $scope.codeStatus = response.data;
                console.log(response.data);
                $window.sessionStorage.setItem('token', headers('Authorization'));
                $window.sessionStorage.setItem('user',JSON.stringify(response.data));
                console.log("from localstorage " + $window.sessionStorage.getItem('token'));

                tokenAuthTest(headers('Authorization'), $state);
            }).
            error(function (response) {      
                // Getting Error Response in Callback           
                $scope.codeStatus = response || "Request failed";

                console.log($scope.codeStatus.ResponseMessage)
                //handle errors thrown by server  
                if ($scope.codeStatus.ResponseMessage == "!verified") {
                    showDialogue("Not verified","Please verify your account first.<br />check your mail for verification code")
                } else {
                    if ($scope.codeStatus.ResponseMessage == "incorect_Password") {
                        showDialogue("Incorect Password","Please enter corect password")
                    } else {
                        if ($scope.codeStatus.ResponseMessage == "email_!found") {
                            showDialogue("Not Registered","Please click on signup if you are not registered");
                        } else {
                            if ($scope.codeStatus.ResponseMessage == "!valid_email") {
                                showDialogue("Invalid email address","Please make sure that email is in correct fomat")
                            } else {
                                showDialogue("server error","Server facing some issues please try again later!");                             
                            }
                        }
                    }
                }
            });
        }
        else{
            showDialogue("fields are empty or have wrong inputs","please fill all the necessary fields with valid data")
        }
        
        function showDialogue(header,message) {
               $scope.log.messageHeader = header;
               $scope.log.mainMessage = message;
               $scope.log.confirm = true;
        }

        $scope.hidedialogue = function () {
            $scope.log.confirm = false;
        }

        function tokenAuthTest(token, $state) {
            $http({
                url: 'http://127.0.0.1:3000/secure',
                method: 'GET',
                headers: { 'Authorization': token }
            }).
                success(function (response) {
                    // Getting Success Response in Callback
                    console.log(response);
                    $rootScope.$emit("permission", {});
                    $state.go('home.timeline');
                }).
                error(function (response) {
                    // Getting Error Response in Callback             
                    $scope.codeStatus = response || "Request failed";
                    $state.go('login');
                });
            $scope.hidedialogue = function () {
                $scope.log.confirm = false;
            }
        }
    };
}]);

pplapp.controller('logoutController', ['localstorage','$scope', '$http', '$window','$state','$rootScope', function (localstorage,$scope, $http, $window,$state,$rootScope) {
    $scope.logout = function () {
        console.log("in logout " + $window.sessionStorage.getItem('token'));
        $http({
            url: 'http://127.0.0.1:3000/secure/logout',
            method: 'GET',
            headers: { 'Authorization': $window.sessionStorage.getItem('token') }
        }).
            success(function (response) {
                // Getting Success Response in Callback
                console.log(response)
                $scope.codeStatus = response.data;
                sessionStorage.removeItem('token');
                
                sessionStorage.removeItem('user');
                $rootScope.$emit("permission", {});
                $state.go('login');
            }).
            error(function (response) {
                // Getting Error Response in Callback             
                $scope.codeStatus = response || "Request failed";
                alert($scope.codeStatus.ResponseMessage);
                sessionStorage.removeItem('user');
                sessionStorage.removeItem('token');
            });
    };
}]);