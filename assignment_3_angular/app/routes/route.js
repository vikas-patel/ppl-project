pplapp.config(function($stateProvider, $urlRouterProvider) {
    
    // $urlRouterProvider.otherwise('/home');
    
    $stateProvider
        .state('home', {
            url: '/home',
            views: {
                '@': { templateUrl: 'index.html' },
                'right@': { templateUrl: '/app/views/rightContent.html',
                            controller:  'rightController' 
                 },
                'left@': { templateUrl: '/app/views/post.html',
                            controller: 'postsController'
                }
            }         
       })
       .state('home.category', {
            url: '/category',
            views: {
               'left@': { templateUrl: '/app/views/post.html',
                            controller: 'postsController'
                 }
            }        
       })
       .state('home.changecategory', {
            url: '/category',
            views: {
               'left@': { templateUrl: '/app/views/post.html',
                            controller: 'postsController'
                 }
            }        
       })
        .state('home.timeline', {
            url: '/timeline',
            views: {
                'left@': { templateUrl: '/app/views/timeline.html',
                            controller:  'timelineController'  }          
            }         
       })
        .state('singlepost', {
            url: '/post',
            views: {
                'right@': { templateUrl: '/app/views/rightContent.html',
                            controller:  'rightController'  },
                'left@': { templateUrl: '/app/views/single_post.html',
                           controller:'singlePostController'
                }
            }         
       })
       .state('login', {
            url: '/login',
            views: {
                'right@': { templateUrl: '/app/views/login.html',
                            controller: 'loginController'
                 },
                'left@': { templateUrl: '/app/views/leftContent.html'}
            }         
       })
       .state('register', {
            url: '/register',
            views: {    
                'right@': { templateUrl: '/app/views/register.html',
                            controller:'registerController'
                 },
                'left@': { templateUrl: '/app/views/leftContent.html',
                           controller:''
                }
            }         
       })
       .state('forgotPassword', {
            url: '/forgotPassword',
            views: {
                'right@': { templateUrl: '/app/views/forgotPassword.html',
                            controller: 'forgotPasswordController'
                 },
                'left@': { templateUrl: '/app/views/leftContent.html' }
            }         
       })
       .state('resetPassword', {
            url: '/resetPassword',
            views: {
                'right@': { templateUrl: '/app/views/resetPassword.html', 
                            controller: 'resetPasswordController'
                  },
                'left@': { templateUrl: '/app/views/leftContent.html' }
            }         
       })
});